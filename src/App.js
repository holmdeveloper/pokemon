import React, { useState, useEffect } from "react";
import { pokemonList, getPokemon} from "./service/PokemonList";
import Card from "./comp/Cards";
import Navbar from "./comp/Navbar";
import { FaRegArrowAltCircleLeft, FaRegArrowAltCircleRight } from "react-icons/fa";
import "./App.css";
import axios from "axios";

/* eslint-disable */
function App() {
  const [pokemonData, setPokemonData] = useState([]);
  const [pokemonName, setPokemonName] = useState("");
  const [pokemonChosen, setPokemonChosen] = useState(false);
  const [error, setError] = useState(false);
  const [count, setCount] =useState("")
  const [errorMsg, setErrorMsg] = useState("");
  const [found, setFound] = useState(false);
  const [foundMsg, setFoundMsg] = useState("");

  const [pokemon, setPokemon] = useState({
    id: "",
    name: "",
    species: "",
    ability: "",
    img: "",
    type: "",
    exp:"",
    hp:"",
    defence:"",
    attack:"",
    special_attack:"",
    special_defense:"",
    speed:"",
  });
  const [nextPageUrl, setNextPageUrl] = useState();
  const [prevPageUrl, setPrevPageUrl] = useState();
  const [loading, setLoading] = useState(true);
  const pokemonUrl = "https://pokeapi.co/api/v2/pokemon/";
  useEffect(() => {
    setLoading(true);
    async function fetchData() {
      let response = await pokemonList(pokemonUrl);
      setNextPageUrl(response.next);
      setPrevPageUrl(response.previous);
      setCount(response.count);
      console.log(response.count)
      await loadingPokemon(response.results);

      setLoading(false);
    }
    fetchData();
  }, []);

  const next = async () => {
    setLoading(true);
    let data = await pokemonList(nextPageUrl);
    await loadingPokemon(data.results);
    setNextPageUrl(data.next);
    setPrevPageUrl(data.previous);
    setLoading(false);
  };

  const prev = async () => {
    if (!prevPageUrl) return;
    setLoading(true);
    let data = await pokemonList(prevPageUrl);
    await loadingPokemon(data.results);
    setNextPageUrl(data.next);
    setPrevPageUrl(data.previous);
    setLoading(false);
  };

  const loadingPokemon = async (data) => {
    let _pokemon = await Promise.all(
      data.map(async (pokemon) => {
        let pokemonRecord = await getPokemon(pokemon.url);
        return pokemonRecord;
      })
    );
    setPokemonData(_pokemon);
  };
  const sePokemon = () => {
    axios
      .get(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`)
      .then((response) => {
        console.log(response)
        setPokemon({
          id: response.data.id,
          name: pokemonName,
          species: response.data.species.name,
          ability: response.data.abilities[0].ability.name,
          img: response.data.sprites.other.dream_world.front_default,
          type: response.data.types[0].type.name,
          exp: response.data.base_experience,
          hp: response.data.stats[0].base_stat,
          defence: response.data.stats[1].base_stat,
          attack: response.data.stats[2].base_stat, 
          special_attack: response.data.stats[3].base_stat,
          special_defense: response.data.stats[4].base_stat,
          speed: response.data.stats[5].base_stat,

        });
        setPokemonChosen(true);
        setFound(true);
        setFoundMsg("You found a Pokémon with that name!");
        setError(false);
      });
    setError(true);
    setErrorMsg("No Pokémon found with that name!");
    setFound(false);
  };
  return (
    <div>
      {loading ? (
        <div className="container">
        <div className="box">
        loading
       </div>
        </div>

      ) : (
        <>
          <div className="container">
            <Navbar />
            {error ? (
              <div class="notification is-warning">{errorMsg}</div>
            ) : null}
            {found ? (
              <div class="notification is-success">{foundMsg}</div>
            ) : null}
            <div className="columns">
              <div className="column">
               
                <input
                  className="input is-rounded"
                  type="text"
                  placeholder="Search....."
                  onChange={(e) => {
                    setPokemonName(e.target.value.toLowerCase());
                  }}
                />
              
              </div>
              <div className="column">
                <div className="s-center">
                <button className="button is-info" onClick={sePokemon}>
                  Search
                </button>
                </div>
                </div>
                <div className="column">
                <div className="s-center">
              <div className="count"> Pokémon: {count} </div>
              </div>
              </div>
              </div>
          
            <div className="box-for-search">
              {!pokemonChosen ? (
                <>
                 <nav className="pagination is-rounded">
                    <button className="pagination-previous" onClick={prev}>
                      <FaRegArrowAltCircleLeft size={20} /> 
                    </button>

                    <button className="pagination-next" onClick={next}>
                   
                   <FaRegArrowAltCircleRight size={20}/>
                    </button>
                  </nav>
                  <div className="columns is-multiline mt-1">
                    {pokemonData.map((pokemon, i) => {
                      return <Card key={i} pokemon={pokemon} />;
                    })}
                  </div>
                  <nav className="pagination is-rounded">
                    <button className="pagination-previous" onClick={prev}>
                      <FaRegArrowAltCircleLeft size={20} /> 
                    </button>

                    <button className="pagination-next" onClick={next}>
                   
                   <FaRegArrowAltCircleRight size={20}/>
                    </button>
                  </nav>
                </>
              ) : (
                <>
                  <div className="card-s">
                  
                    <div className="box">
                    <a href="/" className="button is-primary">
                    <FaRegArrowAltCircleLeft size={20} /> 
                        </a>
                      <div className="columns">
                        <div className="column">
                          <div className="center-1">#{pokemon.id}</div>
                          <div className="name_pokemon_center">{pokemon.name}</div>

                          <div className="size-s">
                            <div>
                            <img src={pokemon.img} />
                            </div>
                          </div>
                        </div>
                        <div className="column">
                          <div className="name_pokemon">
                            Type: {pokemon.type}
                          </div>
                          <div className="name_pokemon">
                            Species: {pokemon.species}
                          </div>
                          <div className="name_pokemon">
                            {" "}
                            ability: {pokemon.ability}

                          </div>
                          <div className="name_pokemon">
                            {" "}
                            exp:  <progress class="progress is-success" value={pokemon.exp} max="255" label={pokemon.exp}></progress>
                            
                          </div>
                          <div className="name_pokemon">
                            {" "}
                            hp:  <progress class="progress is-success" value={pokemon.hp} max="255" label={pokemon.hp}></progress>
                            
                          </div>
                          <div className="name_pokemon">
                            {" "}
                            defence:  <progress class="progress is-success" value={pokemon.defence} max="255" label={pokemon.defence}></progress>
                            
                          </div>
                          <div className="name_pokemon">
                            {" "}
                            attack:  <progress class="progress is-success" value={pokemon.attack} max="255" label={pokemon.attack}></progress>
                            
                          </div>
                          <div className="name_pokemon">
                            {" "}
                            Special-attack:  <progress class="progress is-success" value={pokemon.special_attack} max="255" label={pokemon.special_attack}></progress>
                            
                          </div>
                          <div className="name_pokemon">
                            {" "}
                            Special-defense:  <progress class="progress is-success" value={pokemon.special_defense} max="255" label={pokemon.special_defense}></progress>
                            
                          </div>
                          <div className="name_pokemon">
                            {" "}
                            Speed:  <progress class="progress is-success" value={pokemon.speed} max="255" label={pokemon.speed}></progress>
                            
                          </div>
                        </div>
                        
                      </div>
                    </div>
                  </div>
                </>
              )}
            </div>
          </div>
        </>
      )}
    </div>
  );
}

export default App;
