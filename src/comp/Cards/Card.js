import React, { useState } from "react";
import './styles.css';
import typeHelper from "../../styleHelper/TypeHelper";
import {AiFillCaretUp, AiFillCaretDown} from "react-icons/ai";

function Card({ pokemon }) {
    const [check, setCheck] = useState(true);
    const [openDeck, setOpenDeck] = useState(false);
    const openDecker = () => {
        setOpenDeck((prev) => !prev);
        setCheck(false);
    };
    return (
        <div className="box">
            
                <div onClick={openDecker} className="deck">  
                    {openDeck ? <AiFillCaretUp/> : <AiFillCaretDown/>}
                </div>	
           
           
                <div className="is-one-quarter">
            <div className="box-size">

            <div className="Card__name">#{pokemon.id}</div>
            <div className="Card__name">{pokemon.name}</div>
            {openDeck && (
                <div className="Card__info">
                    <div className="Card__img">
                        <img src={pokemon.sprites.other.dream_world.front_defaultfront_default} alt="" />
                        
                    </div>
                    <div className="Card__types">
                        {pokemon.types.map((type) => {
                            return (
                                <div
                                    className="Card__type"
                                    style={{
                                        backgroundColor:
                                            typeHelper[type.type.name],
                                    }}
                                >
                                    {type.type.name}
                                </div>
                            );
                        })}
                    </div>
                    <div className="Card__info">
                        XP: {pokemon.base_experience}
                    </div>

                    <div className="Card__info">Weight:{pokemon.weight}</div>
                    <div className="Card__info">Height: {pokemon.height}</div>
                    <div className="Card__info">
                        Ability: {pokemon.abilities[0].ability.name}
                    </div>
                    {pokemon.stats.map((stat, key) => (
                <div key={key}>
               <p className="text">{stat.stat.name}</p>
                  <progress class="progress is-success" value={stat.base_stat} max="255" label={stat.base_stat}></progress>
                </div>
              ))}
              

                </div>
            )}

            <div className={openDeck ? "active" : ""}>	
                <div className="Card__img">
                    <img
                        src={
                            check
                                ? pokemon.sprites.other.dream_world.front_default
                                : pokemon.sprites.other.dream_world.front_default
                        }
                        alt={pokemon.name}
                    />
                </div>
            </div>
        </div>
        </div>
        </div>
    );
}

export default Card;